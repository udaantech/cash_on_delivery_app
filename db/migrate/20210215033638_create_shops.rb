class CreateShops < ActiveRecord::Migration[5.2]
  def self.up
    create_table :shops  do |t|
      t.string :shopify_domain, null: false
      t.string :shopify_token, null: false
      t.bigint :charge_id
      t.boolean :charge_cancelled,default: false
      t.timestamp :trial_ends_on
      t.timestamp :billing_on
      t.timestamps
    end

    add_index :shops, :shopify_domain, unique: true
  end

  def self.down
    drop_table :shops
  end
end
