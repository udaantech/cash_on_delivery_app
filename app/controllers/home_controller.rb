class HomeController < AuthenticatedController

  before_action :set_shop

  def index
    @shop = ShopifyAPI::Shop.current
    if params[:type].nil? || ( params[:type] == "unfulfilled" && params[:search].blank? )
      @active = "unfulfilled"
      @orders = ShopifyAPI::Order.find(:all, params: { limit: 2 ,status: 'any',fulfillment_status: 'unfulfilled' })
    elsif (params[:type] == "all" && params[:search].blank?)
      @orders = ShopifyAPI::Order.find(:all, params: { limit: 2 ,status: 'any' })
      @active = "all"
    end
    if params[:token]
      @orders = ShopifyAPI::Order.find(:all, params: { limit: 2,page_info: params[:token] })
    end
  end

  private

  def set_shop
    @current_shop = Shop.find_by_shopify_domain(session[:shopify_domain])
  end

end
