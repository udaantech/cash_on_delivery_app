Rails.application.routes.draw do
  # root :to => 'home#index'
  root :to => 'recurring#create_recurring_application_charge'
  mount ShopifyApp::Engine, at: '/'
  get 'index',to: 'home#index'
  get 'activatecharge',to: 'recurring#activate_charge'
  post '/cancel/charge',to: 'recurring#cancel_charge'
end
